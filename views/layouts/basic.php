<?php
use app\assets\AppAsset;
use yii\helpers\Html;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!doctype html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <?php $this->registerCsrfMetaTags() ?>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

    <div class="wrap">
        <div class="container">
            <ul class="nav nav-pills">
                <li class="nav-item">
                    <?= Html::a('Главная', '/web/'); ?>
                </li>
                <li class="nav-item">
                    <?= Html::a('Статьи', ['post/index']); ?>
                </li>
                <li class="nav-item">
                    <?= Html::a('Статья', ['post/show']); ?>
                </li>
            </ul>

            <?= $content; ?>

        </div>
    </div>

    <h1>Hello, basic.php!</h1>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>