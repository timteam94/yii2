<?php

namespace app\controllers;

use app\models\Category;
use Yii;
use app\models\TestForm;

class PostController extends AppController {

//    public $layout = 'basic';

    public function actionIndex() {
//        if (Yii::$app->request->isAjax){
//            debug($_POST);
//            debug(Yii::$app->request->post());
//            return 'done';
//        }

//        $post = TestForm::findOne(1);
//        $post->email = '45@s.com'; обновление данных в БД
//        $post->save();
//
//        $post->delete();

//        TestForm::deleteAll(['>', 'id', 3]); //удаление данных по условию

        $model = new TestForm();
//        $model->name = 'Author';
//        $model->email = 'a@a.ru';
//        $model->text = 'message';
//        $model->save(); //save() запускает валидацию. отправить false. если валидация не нужна

//        if ( $model->load(Yii::$app->request->post()) ) {
////            debug($model);
////            die;
//            if ( $model->validate() ) { //проверка на валидацию введенных данных
//                Yii::$app->session->setFlash('success', 'Данные приняты'); //создание флеш-сообщения при успешной проверке
//                return $this->refresh(); //очистка страницы
//            } else {
//                Yii::$app->session->setFlash('error', 'Ошибка');
//            }
//        }

        if ( $model->load(Yii::$app->request->post()) ) {
//            debug($model);
//            die;
            if ( $model->save() ) { //проверка на валидацию введенных данных
                Yii::$app->session->setFlash('success', 'Данные приняты'); //создание флеш-сообщения при успешной проверке
                return $this->refresh(); //очистка страницы
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка');
            }
        }

        return $this->render('test', compact('model'));
    }

    public function actionShow() {
        //$this->layout = 'basic';

        //1 шаг: find() - объект запроса
        //2 шаг: - настроить объект запроса (WHERE, ORDER BY, LIMIT, etc...)
        //3 шаг: all() - получение данных
        // SORT_ASC - 1-10; SORT_DESC - 10-1

//        $cats = Category::find()->all(); //запрос в БД на показ всей таблицы SELECT * FROM `posts`
//        $cats = Category::find()->orderBy(['id' => SORT_DESC])->all(); //запрос в БД с сортировкой
//        $cats = Category::find()->asArray()->all(); //asArray - получение данных из БД в виде массива
//        $cats = Category::find()->asArray()->where('id=1')->all(); //вывод с условием WHERE
//        $cats = Category::find()->asArray()->where(['id' => 1])->all(); //
//        $cats = Category::find()->asArray()->where(['like', 'title', 'кто'])->all(); //
//        $cats = Category::find()->asArray()->where(['<=', 'id', 5])->all(); //
//        $cats = Category::find()->asArray()->where(['<=', 'id', 15])->limit(2)->all(); //
//        $cats = Category::find()->asArray()->where(['<=', 'id', 15])->one(); //получение всех записей, но в массиве только одна
//        $cats = Category::find()->asArray()->where(['<=', 'id', 15])->count(); //получение количества записей
//        $cats = Category::find()->asArray()->count(); //получение количества записей

//        $cats = Category::findOne(['id' => 2]); //получение количества записей
//        $cats = Category::findAll(['id' => 2]); //получение количества записей

//        $query = "SELECT * FROM articles WHERE title LIKE '%кто%'";
//        $cats = Category::findBySql($query)->asArray()->all();

//        $query = "SELECT * FROM articles WHERE title LIKE :search";
//        $cats = Category::findBySql($query, [':search' => '%кто%'])->all();

        $cats = Category::findOne(6);

        return $this->render('show', compact('cats'));
    }

}

?>